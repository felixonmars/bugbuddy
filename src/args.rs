use clap::{ArgAction, Parser, Subcommand, ValueHint};
use reqwest::Url;

/// Friendly bug buddy helping with your repo maintenance.
#[derive(Debug, Parser, Clone)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Args {
    /// Verbose logging, specify twice for more
    #[arg(short, long, action = ArgAction::Count)]
    pub verbose: u8,

    /// GitLab URL
    #[arg(
        long,
        env = "BUGBUDDY_GITLAB_URL",
        default_value = "https://gitlab.archlinux.org",
        value_hint = ValueHint::Url,
    )]
    pub gitlab_url: Url,

    /// GitLab group to handle packaging bugs
    #[arg(
        long,
        env = "BUGBUDDY_GITLAB_PACKAGES_GROUP",
        default_value = "archlinux/packaging/packages"
    )]
    pub gitlab_packages_group: String,

    /// GitLab project id for controlling bugbuddy
    #[arg(long, env = "BUGBUDDY_CONTROL_PROJECT_ID", default_value = "65993")]
    pub control_project_id: u64,

    /// GitLab issue iid for controlling bugbuddy
    #[arg(long, env = "BUGBUDDY_CONTROL_ISSUE_IID", default_value = "1")]
    pub control_issue_iid: u64,

    /// URL for mapping maintainers to pkgbase
    #[arg(
        long,
        env = "BUGBUDDY_PKGBASE_MAINTAINER_URL",
        default_value = "https://archlinux.org/packages/pkgbase-maintainer",
        value_hint = ValueHint::Url,
    )]
    pub pkgbase_maintainer_url: Url,

    /// Amount of concurrent project processing
    #[arg(long, env = "BUGBUDDY_CONCURRENT_PROJECTS", default_value = "16")]
    pub concurrent_projects: usize,

    /// Amount of concurrent issue processing
    #[arg(long, env = "BUGBUDDY_CONCURRENT_ISSUES", default_value = "32")]
    pub concurrent_issues: usize,

    #[command(subcommand)]
    pub command: Command,
}

#[derive(Debug, Parser, Clone)]
pub struct DaemonArgs {
    /// Bind daemon to address
    #[arg(long, env = "BUGBUDDY_BIND_ADDRESS", default_value = "[::]")]
    pub bind_address: String,

    /// Bind daemon to port
    #[arg(long, env = "BUGBUDDY_BIND_PORT", default_value = "8080")]
    pub bind_port: u64,

    /// Update maintainer and role data interval in seconds
    #[arg(
        long,
        env = "BUGBUDDY_UPDATE_DATA_INTERVAL_SECONDS",
        default_value = "60"
    )]
    pub update_data_interval_seconds: u64,

    /// Delay between full run over all issues
    #[arg(long, env = "BUGBUDDY_FULL_RUN_DELAY_MINUTES", default_value = "30")]
    pub full_run_delay_minutes: u64,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Command {
    /// Run in single execution mode
    Run,

    /// Start a daemon to listen to webhook calls
    Daemon(DaemonArgs),
}
