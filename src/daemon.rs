use crate::args::DaemonArgs;
use std::env;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{Context, Result};
use axum::extract::{FromRequestParts, State};
use axum::http::request::Parts;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::routing::{get, post};
use axum::{async_trait, Extension, Json, RequestPartsExt, Router};
use crypto_ops::fixed_time_eq;
use log::{error, info};
use tokio::sync::RwLock;

use crate::BugBuddy;
use serde::Deserialize;
use tokio::task;
use tokio::time::sleep;

#[derive(Clone)]
struct ApiSecret {
    token: String,
}

type AppState = Arc<RwLock<AppContext>>;

struct AppContext {
    bot: Arc<BugBuddy>,
}

pub struct Daemon {
    args: DaemonArgs,
    bot: Arc<BugBuddy>,
}

impl Daemon {
    pub async fn new(args: DaemonArgs, bot: Arc<BugBuddy>) -> Result<Self> {
        Ok(Self { args, bot })
    }

    pub async fn start(self) -> Result<()> {
        //let foo: TimedCache<u64, Mutex<()>> = TimedCache::with_lifespan_and_refresh(10, true);

        // initialize tracing
        //tracing_subscriber::fmt::init();

        let api_secret =
            env::var("BUGBUDDY_WEBHOOK_TOKEN").context("Missing env var BUGBUDDY_WEBHOOK_TOKEN")?;
        let api_secret = ApiSecret { token: api_secret };

        let bot_updater = self.bot.clone();
        let bot_updater_handler = task::spawn(async move {
            let mut interval =
                tokio::time::interval(Duration::from_secs(self.args.update_data_interval_seconds));
            interval.tick().await;
            loop {
                interval.tick().await;
                let result = bot_updater.update_data().await;
                if let Err(err) = result {
                    error!("Error: {:?}", err);
                    for cause in err.chain() {
                        error!("Caused by: {:?}", cause)
                    }
                }
            }
        });

        let bot_run = self.bot.clone();
        let bot_run_handler = task::spawn(async move {
            let delay = Duration::from_secs(self.args.full_run_delay_minutes * 60);
            loop {
                let result = bot_run.run(&bot_run).await;
                if let Err(err) = result {
                    error!("Error: {:?}", err);
                    for cause in err.chain() {
                        error!("Caused by: {:?}", cause)
                    }
                }
                sleep(delay).await;
            }
        });

        let shared_state = Arc::new(RwLock::new(AppContext {
            bot: self.bot.clone(),
        }));

        // build our application with a route
        let app = Router::new()
            .route("/", get(root))
            .route("/api/v1/gitlab/webhook", post(webhook))
            .with_state(shared_state)
            .layer(Extension(api_secret));

        let listen = format!("{}:{}", self.args.bind_address, self.args.bind_port);
        info!("Listening on {}", listen);
        axum::Server::bind(&listen.parse()?)
            .serve(app.into_make_service())
            .await?;

        bot_updater_handler.await?;
        bot_run_handler.await?;
        Ok(())
    }
}

// basic handler that responds with a static string
async fn root() -> &'static str {
    "Hello, World!"
}

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "object_kind")]
#[serde(rename_all = "snake_case")]
pub enum Webhook {
    Issue(IssueEvent),
    Note(NoteEvent),
    MergeRequest(MergeRequestEvent),
}

#[derive(Clone, Debug, Deserialize)]
pub struct IssueEvent {
    project: Project,
    object_attributes: Issue,
}

#[derive(Clone, Debug, Deserialize)]
pub struct NoteEvent {
    project_id: u64,
    issue: Option<Issue>,
    merge_request: Option<MergeRequest>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct MergeRequestEvent {
    object_attributes: MergeRequest,
    project: Project,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Project {
    id: u64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Issue {
    iid: u64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct MergeRequest {
    iid: u64,
}

struct CheckGitLabAPIToken();
const X_GITLAB_TOKEN: &str = "X-Gitlab-Token";

#[async_trait]
impl<S> FromRequestParts<S> for CheckGitLabAPIToken
where
    S: Send + Sync,
{
    type Rejection = (StatusCode, &'static str);

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let Extension(api_secret) = parts
            .extract::<Extension<ApiSecret>>()
            .await
            .map_err(|e| (e.into_response().status(), "failed to extract api secret"))?;

        if let Some(token) = parts.headers.get(X_GITLAB_TOKEN) {
            if fixed_time_eq(
                token.to_str().unwrap_or_default().as_bytes(),
                api_secret.token.as_bytes(),
            ) {
                Ok(CheckGitLabAPIToken())
            } else {
                Err((StatusCode::FORBIDDEN, "`X-Gitlab-Token` header is invalid"))
            }
        } else {
            Err((StatusCode::FORBIDDEN, "`X-Gitlab-Token` header is missing"))
        }
    }
}

async fn webhook(
    _token: CheckGitLabAPIToken,
    State(state): State<AppState>,
    Json(event): Json<Webhook>,
) -> Result<&'static str, (StatusCode, String)> {
    info!("Got a webhook event: {:?}", event);

    let state = state.read().await;
    let bot = state.bot.clone();

    match event {
        Webhook::Issue(issue) => {
            task::spawn(async move {
                bot.process_issue_id_logged(issue.project.id, issue.object_attributes.iid)
                    .await
            });
        }
        Webhook::MergeRequest(merge_request) => {
            task::spawn(async move {
                bot.process_merge_request_id_logged(
                    merge_request.project.id,
                    merge_request.object_attributes.iid,
                )
                .await
            });
        }
        Webhook::Note(note) => {
            if let Some(issue) = note.issue {
                task::spawn(async move {
                    bot.process_issue_id_logged(note.project_id, issue.iid)
                        .await
                });
            } else if let Some(merge_request) = note.merge_request {
                task::spawn(async move {
                    bot.process_merge_request_id_logged(note.project_id, merge_request.iid)
                        .await
                });
            }
        }
    }

    Ok("update scheduled")
}
