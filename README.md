# Bugbuddy

## Automatic behavior

### Assignees

#### Package Maintainer

Assigns all package maintainers retrieved from ArchWeb to all open issues that
are not `status::unconfirmed`. Furthermore also assigns all package maintainers
to all open merge requests.

Whenever a package maintainer disown a package, they will also be removed from
the assignees of that packaging repos issues and merge requests.

#### Bug Wrangler

Assigns all bug wranglers listed in the GitLab Team to all open issues that are
`status::unconfirmed`.

Once the status label is changed to `status::confirmed`, all Bug Wranglers will
automatically get unassigned from the issue.

#### Other

To assign other assignees than Package Maintainer or Bug Wrangler use the custom
commands. This can be useful if someone else is a subject-matter expert for a
certain problem.

## Labels

All newly created issues and merge requests without a status label will get the
`status::unconfirmed` label assigned. This allows for conveniently searching
for the `status::unconfirmed` label or even subscribing to it.

Furthermore, default labels are assigned to issues and merge requests as follows.

Issues:
- `priority::3-normal`
- `severity::4-low`

Merge Requests:
- `priority::3-normal`

## Custom commands

Notes can be used to control the bot via special commands. The notes are
read in order and may overwrite each other. All commands must be directed to
bugbuddy by mentioning them in the first token of the note via `@bugbuddy`.

Access control is achieved by only allowing staff members to use bugbuddy
commands.

### Assign/Unassign

access: staff

The `assign` and `unassign` commands can be used to control none maintainer
assignees.

```
@bugbuddy assign @anthraxx @foobar

Futher lines can be used as description why this assignment has been made.
```

## Global per-user settings

React with the emojis described in [this
issue](https://gitlab.archlinux.org/archlinux/bugbuddy/-/issues/1) to activate
per user settings:

## Setup

### Environment variables

The runtime uses the following environment variables:

#### Required

| variable               | component | usage                                    |
|------------------------|-----------|------------------------------------------|
| BUGBUDDY_GITLAB_TOKEN  | all       | GitLab API token of the bot user         |
| BUGBUDDY_WEBHOOK_TOKEN | daemon    | Shared secret for the GitLab webhook API |

#### Optional

Global settings for one-shot run as well as daemon mode:

| variable                              | usage                                    | default                                           |
|---------------------------------------|------------------------------------------|---------------------------------------------------|
| BUGBUDDY_CONTROL_PROJECT_ID           | Per user settings project id             | 65993                                             |
| BUGBUDDY_CONTROL_ISSUE_IID            | Per user settings issue iid              | 1                                                 |
| BUGBUDDY_GITLAB_URL                   | URL of the GitLab instance               | https://gitlab.archlinux.org                      |
| BUGBUDDY_GITLAB_PACKAGES_GROUP        | GitLab group to handle packaging bugs    | archlinux/packaging/packages                      |
| BUGBUDDY_PKGBASE_MAINTAINER_URL       | URL for mapping maintainers to pkgbase   | https://archlinux.org/packages/pkgbase-maintainer |
| BUGBUDDY_CONCURRENT_PROJECTS          | Number of concurrent project processing  | 16                                                |
| BUGBUDDY_CONCURRENT_ISSUES            | Number of concurrent issue processing    | 32                                                |

Settings exclusive to the daemon mode:

| variable                              | usage                                    | default |
|---------------------------------------|------------------------------------------|---------|
| BUGBUDDY_BIND_ADDRESS                 | Bind daemon to address                   | [::]    |
| BUGBUDDY_BIND_PORT                    | Bind daemon to port                      | 8080    |
| BUGBUDDY_UPDATE_DATA_INTERVAL_SECONDS | Update maintainer and role data interval | 60      |
| BUGBUDDY_FULL_RUN_DELAY_MINUTES       | Delay between full run over all issues   | 30      |
